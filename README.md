# Devops Jenkins Server

## Prepare environment

Edit .env file accordingly

```
cp .env.example .env
vi .env
```

## How to Run

```
# docker
docker compose up

# podman-compose
podman-compose up
```

### How to Stop

```
# docker
docker compose down

# podman-compose
podman-compose down
```
